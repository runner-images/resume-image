FROM ruby:2.6-alpine
RUN apk add texlive-full
RUN apk add ruby-dev
RUN apk add --update \
  build-base \
  libxml2-dev \
  libxslt-dev \
  postgresql-dev \
  && rm -rf /var/cache/apk/*
COPY . .
RUN bundle install